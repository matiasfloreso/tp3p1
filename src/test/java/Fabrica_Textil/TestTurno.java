package Fabrica_Textil;

import org.junit.Test;
import static org.junit.Assert.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TestTurno {

    
    @Test
    public void testAgregarEmpleado(){
        
        DateTimeFormatter formatoFecha = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        Turno turno1= new Turno(LocalDateTime.parse("14/08/2020 06:00:00",formatoFecha), LocalDateTime.parse("14/08/2020 12:00:00",formatoFecha));
        Empleado empleado1=new Empleado("matias","flores",8,111111);
        Empleado vectorempleado [] = new Empleado[10];
        turno1.agregarEmpleado(empleado1);
        vectorempleado[0]=empleado1;


        assertArrayEquals(vectorempleado, turno1.getVectorEmpleado());
    }
    
}