package Fabrica_Textil;

import jdk.internal.vm.PostVMInitHook;
import jdk.javadoc.internal.doclets.formats.html.SourceToHTMLConverter;

public class Empleado{

    private String nombre;
    private String apellido;
    private float costoHora;
    private Turno turno [];
    private int dni;

    public Empleado(String nombre, String apellido, float costoHora,int dni){
        this.nombre = nombre;
        this.apellido = apellido;
        this.costoHora = costoHora;
        this.dni = dni;
        turno = new Turno [30];
    }
    
    public void agregarTurno(Turno turnoPorAgregar){

        int i=0;
        for (Turno turnoVar : turno) {
            if(turnoVar!=null){
                turno[i]=turnoPorAgregar;
                break;
            }
            i++;

        }
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public String getApellido(){
        return apellido;
    }

    public void setCostoHora(float costoHora){
        this.costoHora = costoHora;
    }

    public float getCostoHora(){
        return costoHora;
    }

    public void setDni(int dni){
        this.dni = dni;
    }

    public int getDni(){
        return dni;
    }

    public String toString(){
        
        return "Nombre del empleado: "+ apellido + " " + nombre + " , Costo por hora: "+ costoHora ; 
    }
}