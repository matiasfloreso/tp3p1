package Fabrica_Textil;

import java.time.Duration;
import java.time.LocalDateTime;

public class Turno{

    //Fecha y horas son de tipo LocalDateTime. (con localdatetime van incluido las fecha y hora)
    private LocalDateTime fechaHoraIngreso;
    private LocalDateTime fechaHoraEgreso;
    private Empleado [] empleado ;

    public Turno ( LocalDateTime fechaHoraIngreso, LocalDateTime fechaHoraEgreso){
        this.fechaHoraIngreso = fechaHoraIngreso;
        this.fechaHoraEgreso = fechaHoraEgreso;
        empleado = new Empleado[10];
    }

    public void agregarEmpleado(Empleado empleadoPorAgregar){

        int i=0;

        for (Empleado var : empleado) {

            if(var.getDni() == empleadoPorAgregar.getDni()){
                
                System.out.println("El empleado que se quiere agregar ya esta listado");
                break;
            }
            
            empleado[i] = empleadoPorAgregar;
            i++;
            
        }

    }
    
    public void eliminarEmpleado(int dni){
        
        int i=0;

        for(Empleado var : empleado){
            
            if(var.getDni() == dni){

                empleado[i]=null;
            }

            i++;
        }
    }

    public void setFechaHoraIngreso(LocalDateTime fechaHoraIngreso) {
        this.fechaHoraIngreso = fechaHoraIngreso;
    }

    public LocalDateTime getFechaHoraIngreso(){
        return fechaHoraIngreso;
    }
    
    public void setFechaHoraEgreso(LocalDateTime fechaHoraEgreso){
        this.fechaHoraEgreso=fechaHoraEgreso;
    }

    public LocalDateTime getFechaHoraEgreso(){
        return fechaHoraEgreso;
    }

    public Empleado[] getVectorEmpleado(){
        int i=0;
        /*for(Empleado var: empleado){
            if(var!=null){
                return empleado[i];
            }
            i++;
        }*/
        return empleado;
    }
}